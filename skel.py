import argparse
import logging
import time
import ast

import common
import cv2
import numpy as np
from estimator import TfPoseEstimator
from networks import get_graph_path, model_wh

def get_coords(npimg):

  image_h, image_w = npimg.shape[:2]

  humans = e.inference(npimg, scales=[None])
  centers = {}
  for human in humans:
    for i in range(common.CocoPart.Background.value):
      if i not in human.body_parts.keys():
        continue

      body_part = human.body_parts[i]
      center = (
      int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
      centers[i] = center
  print(centers)

if __name__ == '__main__':
  w, h = model_wh('mobilenet_thin_432x368')
  e = TfPoseEstimator(get_graph_path('mobilenet_thin_432x368'), target_size=(w, h))
  for i in range(1):
    t = time.time()
    img = cv2.imread('/home/alessio/Sandbox/NTU/frames/S001C001P001R001A001/' + str(i + 1).zfill(4) + '.jpg', cv2.IMREAD_COLOR)
    get_coords(img)
    elapsed = time.time() - t
    print(elapsed)