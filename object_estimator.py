import numpy as np
import tensorflow as tf


class TfObjectEstimator:

  def __init__(self, graph_path):

    # load graph
    with tf.gfile.GFile(graph_path, 'rb') as f:
      graph_def = tf.GraphDef()
      graph_def.ParseFromString(f.read())

    self.graph = tf.get_default_graph()
    tf.import_graph_def(graph_def, name='TfObjectEstimator')

    session_config = tf.ConfigProto()
    session_config.gpu_options.allow_growth = True

    self.persistent_sess = tf.Session(graph=self.graph, config=session_config)
    self.persistent_sess.run(tf.global_variables_initializer())

    self.tensor_input = self.graph.get_tensor_by_name('TfObjectEstimator/image_tensor:0')
    self.tensor_output = [self.graph.get_tensor_by_name('TfObjectEstimator/num_detections:0'),
                          self.graph.get_tensor_by_name('TfObjectEstimator/detection_scores:0'),
                          self.graph.get_tensor_by_name('TfObjectEstimator/detection_boxes:0'),
                          self.graph.get_tensor_by_name('TfObjectEstimator/detection_classes:0')]

    # warm-up
    self.persistent_sess.run(
        self.tensor_output,
        feed_dict={self.tensor_input: [np.ndarray(shape=(300, 300, 3), dtype=np.uint8)]}
    )

  def __del__(self):
    self.persistent_sess.close()

  def inference(self, img):
    return self.persistent_sess.run(self.tensor_output, feed_dict={self.tensor_input: [img]})
