import uuid
import numpy as np
from scipy.optimize import linear_sum_assignment
from sklearn.metrics.pairwise import chi2_kernel, additive_chi2_kernel
from collections import deque

class Track:

  def __init__(self,
               detection,
               histogram,
               frame_height=480,
               frame_width=640,
               tolerance=5,
               history_length=15,
               classes=60):

    self.coords = deque([detection], maxlen=history_length)
    self.box = np.expand_dims(pose_to_box(detection, image_w=frame_width, image_h=frame_height), axis=0)
    self.crop_box = [int(self.box[0, 0]), int(self.box[0, 1]), int(self.box[0, 2]), int(self.box[0, 3])]  # best square box
    self.center = np.expand_dims(np.mean(self.box.reshape(2,2), axis=0), axis=0)
    self.histogram = histogram
    self.image_w = frame_width
    self.image_h = frame_height
    self.tolerance = tolerance
    self.age = int(1)  # number of updates since first detection
    self.seen = int(1)  # number of detections
    self.unseen = int(0)  # number of missed detections
    self.history_length = history_length
    self.actions = np.zeros((3, classes), dtype=np.float32)
    self.box_color = np.random.choice(255, 3, replace=True)


  def update_track(self, detection, histogram):
    self.coords.append(detection)
    self.update_box(pose_to_box(detection, image_w=self.image_w, image_h=self.image_h))
    self.update_histogram(histogram)
    self.increment_seen()

  def update_box(self, new_box):

    # Append last detection box to the 'FIFO queue'
    if self.age < self.history_length:
      self.box = np.concatenate((self.box, [new_box]))
    else:
      self.box = np.concatenate((self.box[1:], [new_box]))

    # Define a 'minimal_box' as the smallest box containing the box of each frame
    minimal_box = np.array([np.amin(self.box[:, 0]), np.amin(self.box[:, 1]), np.amax(self.box[:, 2]), np.amax(self.box[:, 3])])
    self.crop_box = self.calc_optimal_box(minimal_box, self.image_w, self.image_h)
    self.update_center()


  def update_center(self):
    new_center = np.array([(self.box[-1, 0] + self.box[-1, 2]) / 2, (self.box[-1, 1] + self.box[-1, 3]) / 2])
    if self.age < self.history_length:
      self.center = np.concatenate((self.center, [new_center]))
    else:
      self.center = np.concatenate((self.center[1:], [new_center]))

  def update_histogram(self, new_hist, weight=0.4):
    self.histogram = (1 - weight) * self.histogram + weight * new_hist


  def increment_seen(self):
    self.age += 1
    self.seen += 1
    self.unseen = 0

  def increment_unseen(self):
    self.age += 1
    self.unseen += 1
    self.seen = 0

  @staticmethod
  def calc_optimal_box(minimal_box, image_w, image_h, pad=np.array([0.8, 0.9, 1.1, 1.1])):

    padded_box = minimal_box * pad  # more margin on the head since top keypoint is only on the nose
    padded_box = np.clip(padded_box, a_min=[0, 0, 0, 0],
                         a_max=[image_h, image_w, image_h, image_w])

    [y_min, x_min, y_max, x_max] = padded_box

    w = x_max - x_min
    h = y_max - y_min
    delta = abs(w - h) / 2

    if w > h:  # landscape, check if the image is high enough
      if w <= image_h:
        # Check if there is enough space above and below the box
        if int(y_min - delta) >= 0 and int(
            y_max + delta) <= image_h - 1:  # then you can center the crop
          crop_box = [int(y_min - delta), int(x_min), int(y_max + delta),
                      int(x_max)]
        elif int(y_min - delta) >= 0:  # more space above and not enough below
          overcrop = y_max + delta - (image_h - 1)
          crop_box = [int(y_min - delta - overcrop), int(x_min), image_h - 1,
                      int(x_max)]
        else:  # more space below and not enough above
          overcrop = -(y_min - delta)
          crop_box = [0, int(x_min), int(y_max + delta + overcrop), int(x_max)]
      else:  # take the full height as box size
        delta = (w - image_h) / 2
        crop_box = [0, int(x_min + delta), image_h - 1, int(x_max - delta)]
    else:  # portrait, check if the image is wide enough
      if h <= image_w:
        if int(x_min - delta) >= 0 and int(
            x_max + delta) <= image_w - 1:  # then you can center the crop
          crop_box = [int(y_min), int(x_min - delta), int(y_max),
                      int(x_max + delta)]
        elif int(
            x_min - delta) >= 0:  # more space on the left and not enough on the right
          overcrop = x_max + delta - (image_w - 1)
          crop_box = [int(y_min), int(x_min - delta - overcrop), int(y_max),
                      image_w]
        else:  # more space on the right and not enough on the left
          overcrop = -(x_min - delta)
          crop_box = [int(y_min), 0, int(y_max), int(x_max + delta + overcrop)]
      else:  # take the full width as box size
        delta = (h - image_w) / 2
        crop_box = [int(y_min + delta), 0, int(y_max - delta), image_w]
    return crop_box


class TrackList:

  def __init__(self,
               frame_height=480,
               frame_width=640,
               tolerance=5,
               history_length=15,
               hist_bins_per_channel=16,
               classes=60):

    self.track_list = {}
    self.image_h = frame_height
    self.image_w = frame_width
    self.tolerance = tolerance  # after this number of missed detections delete the track
    self.history_length = history_length
    self.hist_bins_per_channel = hist_bins_per_channel
    self.classes = classes

  def length(self):
    return len(self.track_list)

  def keys(self):
    return self.track_list.keys()

  def __getitem__(self, item):
    return self.track_list[item]

  def update_tracks(self, detections, frame):

    # Assignment problem
    threshold = 100.0  # distance/cost from which detection and track cannot be matched
    track_ids = list(self.track_list.keys())
    costs = threshold * np.ones((len(detections), len(self.track_list)), dtype=np.float32)
    # print(len(detections), 'detections /', len(self.track_list), 'tracks')
    detect_hist = np.empty((0, self.hist_bins_per_channel * 3), dtype=np.float32)
    tracks_hist = np.empty((0, self.hist_bins_per_channel * 3), dtype=np.float32)
    for d in detections:
      # Filter detections with low confidence or too few joints
      if self.check_detection(d) == False:
        detections.remove(d)
    for d in detections:
      detect_hist = np.concatenate((detect_hist, [self.get_histogram(d, frame, self.hist_bins_per_channel)]), axis=0)
    # print('detect_hist', detect_hist)
    t_counter = 0
    for t in track_ids:
      tracks_hist = np.concatenate((tracks_hist, [self.track_list[t].histogram]), axis=0)
      t_counter += 1
    # print('tracks_hist', tracks_hist)
    if len(detections) > 0 and len(self.track_list) > 0:
      costs = -additive_chi2_kernel(detect_hist, tracks_hist)
    r, c = linear_sum_assignment(costs)
    assignments = np.squeeze(np.dstack((r, c)), 0)

    # Indices of detections and tracks that are not in any assignments pair
    orphan_detections = np.arange(len(detections))[
      np.in1d(np.arange(len(detections)), r, invert=True)]
    orphan_tracks = np.arange(len(self.track_list))[
      np.in1d(np.arange(len(self.track_list)), c, invert=True)]

    # Update tracks with assigned detections
    for a in assignments:
      if costs[a[0], a[1]] < threshold:
        self.track_list[track_ids[a[1]]].update_track(detections[a[0]], detect_hist[a[0]])
      else:
        orphan_detections = np.concatenate((orphan_detections, [a[0]]))
        orphan_tracks = np.concatenate((orphan_tracks, [a[1]]))

    # Add track for new unassigned detections
    for d in orphan_detections:
      self.track_list[uuid.uuid4()] = Track(detection=detections[d],
                                            histogram=detect_hist[d],
                                            frame_height=self.image_h,
                                            frame_width=self.image_w,
                                            tolerance=self.tolerance,
                                            history_length=self.history_length,
                                            classes=self.classes)

    # Delete track without assigned detection
    for t in orphan_tracks:
      if self.track_list[track_ids[t]].unseen < self.tolerance:
        self.track_list[track_ids[t]].increment_unseen()
      else:
        del self.track_list[track_ids[t]]


  def check_detection(self, detection):
    box = pose_to_box(detection, image_w=self.image_w, image_h=self.image_h)
    if int(box[2]) - int(box[0]) < 1 or int(box[3]) - int(box[1]) < 1:
      return False
    else:
      return True


  def get_histogram(self, detection, frame, nbins):
    # Try to select only the upper-body, from shoulders to hips
    if 2 in detection.body_parts.keys() and 11 in detection.body_parts.keys()\
        and abs((detection.body_parts[2].x - detection.body_parts[11].x) * self.image_w) > 1. \
        and abs((detection.body_parts[2].y - detection.body_parts[11].y) * self.image_h) > 1.:
      box = np.array([min(detection.body_parts[2].y * self.image_h, detection.body_parts[11].y * self.image_h),
                      min(detection.body_parts[2].x * self.image_w, detection.body_parts[11].x * self.image_w),
                      max(detection.body_parts[2].y * self.image_h, detection.body_parts[11].y * self.image_h),
                      max(detection.body_parts[2].x * self.image_w, detection.body_parts[11].x * self.image_w)])
      # print('2-11', box)
    elif 5 in detection.body_parts.keys() and 8 in detection.body_parts.keys()\
        and abs((detection.body_parts[5].x - detection.body_parts[8].x) * self.image_w) > 1. \
        and abs((detection.body_parts[5].y - detection.body_parts[8].y) * self.image_h) > 1.:
      box = np.array([min(detection.body_parts[5].y * self.image_h, detection.body_parts[8].y * self.image_h),
                      min(detection.body_parts[8].x * self.image_w, detection.body_parts[5].x * self.image_w),
                      max(detection.body_parts[5].y * self.image_h, detection.body_parts[8].y * self.image_h),
                      max(detection.body_parts[8].x * self.image_w, detection.body_parts[5].x * self.image_w)])
      # print('5-8', box)
    else:
      box = pose_to_box(detection, image_w=self.image_w, image_h=self.image_h)
      # print('full', box)
    crop_frame = frame[int(box[0]):int(box[2]), int(box[1]):int(box[3]), :]
    hist_R, _ = np.histogram(crop_frame[0], bins=nbins, density=True)
    hist_G, _ = np.histogram(crop_frame[1], bins=nbins, density=True)
    hist_B, _ = np.histogram(crop_frame[2], bins=nbins, density=True)
    histogram = np.concatenate((hist_R, hist_G, hist_B), axis=0)
    return histogram


def pose_to_box(human, image_w=640, image_h=480):
  min_x = min_y = np.inf
  max_x = max_y = 0
  for i in range(18):
    if i not in human.body_parts.keys():
      continue
    body_part = human.body_parts[i]
    if body_part.x * image_w > max_x:
      max_x = body_part.x * image_w
    if body_part.x * image_w < min_x:
      min_x = body_part.x * image_w
    if body_part.y * image_h > max_y:
      max_y = body_part.y * image_h
    if body_part.y * image_h < min_y:
      min_y = body_part.y * image_h

  # TODO: scale considering limbs
  return np.array([min_y, min_x, max_y, max_x])