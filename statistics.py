import os

dataset_path = '/home/alessio/Sandbox/NTU/nturgb+d_rgb'

def dataset_balance():
  samples = 40 * [0]
  locations = 17 * [0]
  actions = 60 * [0]

  for filename in os.listdir(dataset_path):
    sub = int(filename[9:12])
    samples[sub - 1] = samples[sub - 1] + 1
    loc = int(filename[1:4])
    locations[loc - 1] = locations[loc - 1] + 1
    act = int(filename[17:20])
    actions[act - 1] = actions[act - 1] + 1

  print('Samples per subject', samples)
  print('Samples per location', locations)
  print('Samples per action', actions)


def dataset_length():

  lengths = {}
  counter = 0
  for dirname in os.listdir('/home/alessio/Sandbox/NTU/frames'):
    n = len(os.listdir('/home/alessio/Sandbox/NTU/frames/' + dirname))
    if n in lengths:
      lengths[n] += 1
    else:
      lengths[n] = 1
    counter += 1
    print(counter, n)

  print(lengths)


def list_balance(filename, classes=60):
  actions = classes * [0]

  lines = list(open(filename, 'r'))
  for i in range(len(lines)):
    line = lines[i].strip('\n').split()
    label = int(line[1])
    actions[label] = actions[label] + 1

  print(actions)


if __name__ == '__main__':
  list_balance('/home/alessio/Sandbox/NTU/lists/training_list_reduced.txt', classes=20)