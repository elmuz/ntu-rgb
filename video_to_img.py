import os
import argparse
import cv2

dataset_path = '/home/alessio/Sandbox/NTU'

width = 512
height = 384


def process_video(m):

  counter = 0

  # for filename in os.listdir(os.path.join(dataset_path, 'nturgb+d_rgb')):
  for filename in ['S001C001P001R001A001_rgb.avi']:

    if True:

      counter += 1

      cap = cv2.VideoCapture(os.path.join(dataset_path, 'nturgb+d_rgb', filename))
      length = cap.get(7)
      # width_orig = cap.get(3)
      # height_orig = cap.get(4)

      filename = filename[:20]
      print('Processing video', filename, counter)
      os.mkdir(os.path.join(dataset_path, 'frames_center_crop', filename))

      for f in range(int(length)):
        _, frame = cap.read()

        # Center crop to 4:3 aspect ratio [1024x768]
        frame = frame[:, (1920 - 1440) // 2:(1920 - 1440) // 2 + 1440, :]
        # frame = cv2.resize(frame, (width, height))

        cv2.imwrite(os.path.join(dataset_path, 'frames_center_crop', filename, str(f).zfill(4) + '.jpg'), frame)

      cap.release()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Convert video sample to single frames')
  parser.add_argument('--group', type=int, default=0, help='scene group')
  args = parser.parse_args()
  process_video(args.group)