from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import tensorflow as tf
from input_functions import pack_frames, get_box
from i3d import InceptionI3d
from estimator import TfPoseEstimator
import cv2

def main():
  _CHECKPOINT_PATHS = {
    'rgb': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/rgb_scratch/model.ckpt',
    'flow': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/flow_scratch/model.ckpt',
    'rgb_imagenet': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/rgb_imagenet/model.ckpt',
    'flow_imagenet': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/flow_imagenet/model.ckpt',
  }

  # Dataset
  dataset = 'NTU-RGBD'
  experiments_dir = os.path.join('/home/alessio/Sandbox/experiments', dataset)
  classes_file = '/home/alessio/Sandbox/NTU/classes.csv'
  classes = np.genfromtxt(fname=classes_file, dtype=str, delimiter=',', usecols=[0])
  num_classes = 59
  num_frames_per_clip = 32
  crop_size = 224
  img_channels = 3


  # PROVA
  num_prova = 15
  suffix = 'PROVA_'

  # Evaluation parameter
  batch_size = 10
  dropout_keep_prob = 1.0

  # Prepare the model
  pose_estimator = TfPoseEstimator('/home/alessio/Sandbox/dev/OpenPose-TensorFlow/models/graph/mobilenet_thin/graph_opt.pb', target_size=(432, 368))
  rgb_input = tf.placeholder(tf.float32, shape=(None, num_frames_per_clip, crop_size, crop_size, img_channels))
  Y = tf.placeholder(tf.int64, [None])

  with tf.variable_scope('RGB'):
    # rgb_model = InceptionI3d(num_classes=num_classes, spatial_squeeze=True, final_endpoint='Logits')
    rgb_model = InceptionI3d(num_classes=num_classes, spatial_squeeze=True, final_endpoint='Mixed_5c')
  # logits, _ = rgb_model(rgb_input, is_training=False, dropout_keep_prob=dropout_keep_prob)
  mixed_5c, _ = rgb_model(rgb_input, is_training=False, dropout_keep_prob=dropout_keep_prob)
  features_op = tf.squeeze(tf.nn.avg_pool3d(mixed_5c, ksize=[1, 4, 7, 7, 1], strides=[1, 1, 1, 1, 1], padding='VALID'))

  fine_tuning_var_map = {}
  restore_weights_map = {}
  for variable in tf.global_variables():
    if variable.name.split('/')[2] == 'Logits' or \
      variable.name.split('/')[2] == 'Mixed_5c' or \
      variable.name.split('/')[2] == 'Mixed_5b':  # or \
      # variable.name.split('/')[2] == 'Mixed_4f' or \
      # variable.name.split('/')[2] == 'Mixed_4e':
      fine_tuning_var_map[variable.name.replace(':0', '')] = variable
    else:  # variable.name.split('/')[2] != 'Logits':
      restore_weights_map[variable.name.replace(':0', '')] = variable
  rgb_saver = tf.train.Saver(var_list=restore_weights_map, reshape=True)
  # probabilities = tf.nn.softmax(logits)

  # Evaluation metrics
  # cnf_matrix = np.zeros((num_classes, num_classes), dtype=np.int32)
  # predictions = tf.argmax(probabilities, 1)
  # correct_pred = tf.equal(predictions, Y)
  # confusion = tf.confusion_matrix(Y, predictions, num_classes=num_classes)

  # Summaries
  # tf.summary.image()
  # merged_summary = tf.summary.merge_all()



  with tf.Session() as sess:

    filenames = {}
    labels = {}
    lengths = {}
    num_samples = {}

    np_data_train = np.load(os.path.join('/home/alessio/Sandbox/NTU', 'lists', 'training_list_anomaly.npy'))
    filenames['train'] = np_data_train['file']
    labels['train'] = np_data_train['label']
    lengths['train'] = np_data_train['frames']
    num_samples['train'] = np.shape(filenames['train'])[0]

    np_data_test = np.load(os.path.join('/home/alessio/Sandbox/NTU', 'lists', 'testing_list_anomaly.npy'))
    filenames['test'] = np_data_test['file']
    labels['test'] = np_data_test['label']
    lengths['test'] = np_data_test['frames']
    num_samples['test'] = np.shape(filenames['test'])[0]

    np_data_anomaly = np.load(os.path.join('/home/alessio/Sandbox/NTU', 'lists', 'falling_list_anomaly.npy'))
    filenames['anomaly'] = np_data_anomaly['file']
    labels['anomaly'] = np_data_anomaly['label']
    lengths['anomaly'] = np_data_anomaly['frames']
    num_samples['anomaly'] = np.shape(filenames['anomaly'])[0]


    def prepare_progressive_batch(split, last_id=0, s_frame=0, strategy='progressive', flip=False):

      # video_idx = np.arange(last_id, min(last_id + batch_size, num_samples[split]))
      video_id = last_id
      batch_video = np.empty((0, num_frames_per_clip, crop_size, crop_size, 3), dtype=np.float32)
      batch_labels = []
      start_frame = s_frame
      for i in range(batch_size):

        video_cube, label, _, start_frame = pack_frames(filenames[split][video_id],
                                                        labels[split][video_id],
                                                        lengths[split][video_id],
                                                        num_frames_per_clip,
                                                        strategy=strategy,
                                                        s_frame=start_frame,
                                                        hop_size=num_frames_per_clip//2)
        box = get_box(video_cube, pose_estimator)
        video_cube = video_cube[:, box[0]:box[2], box[1]:box[3], :]
        cube_resized = np.empty((0, crop_size, crop_size, 3), np.float32)
        if flip:
          horizontal_flip = np.random.randint(2)  # Randomly flip the video
        else:
          horizontal_flip = False
        for j in range(num_frames_per_clip):
          frame_resized = cv2.resize(video_cube[j], (crop_size, crop_size))
          frame_resized = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2RGB)
          if horizontal_flip:
            cv2.flip(frame_resized, 0)
          cube_resized = np.concatenate((cube_resized, [frame_resized.astype(np.float32)]), axis=0)
        batch_video = np.concatenate((batch_video, [cube_resized]), axis=0)
        batch_labels.append(label)

        # Check if we need to increment video_id
        if start_frame == -1 or strategy != 'progressive':
          if video_id < num_samples[split] - 1:
            video_id += 1
            start_frame = 0
          else:
            video_id = -1
            break

      batch_video = batch_video / 127. - 1.

      return batch_video, batch_labels, video_id, start_frame


    # Add ops to save and restore all the variables.
    classification_saver = tf.train.Saver(var_list=fine_tuning_var_map)

    # Recover last checkpoints (if any) for classification filters, moving averages, ...
    last_checkpoint_file = tf.train.latest_checkpoint(
        checkpoint_dir=os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova)),
        latest_filename='I3D_finetuning_rgb_' + str(num_prova) + '_latest')

    last_step = 0
    if last_checkpoint_file != None:
      last_step = last_checkpoint_file.split('-')[-1]
      print('Found the following checkpoint:', last_checkpoint_file)
      print('Last iteration was', last_step)

    # Run the initializer
    sess.run(tf.global_variables_initializer())

    # Restore al trained model and last checkpoints (if any)
    rgb_saver.restore(sess, _CHECKPOINT_PATHS['rgb_imagenet'])
    print('RGB branch variables successfully restored.')
    if last_checkpoint_file != None:
      classification_saver.restore(sess, last_checkpoint_file)
      print('Classification variables successfully restored.')
    # if last_checkpoint_file_ema != None:
    #   ema_saver.restore(sess, last_checkpoint_file_ema)
    #   print('Moving average variables successfully restored.')

    splits = ['anomaly', 'test', 'train']

    for repeat in range(10):

      for split in splits:

        features = np.empty((0, 1024), dtype=np.float32)
        next_id = 0
        next_frame = 0
        counter = 1
        while next_id >= 0:

          print('Split:', split, '/ ID:', next_id, '[' + str(next_frame) + ']')
          batch_video, _, next_id, next_frame = prepare_progressive_batch(split, last_id=next_id, s_frame=next_frame, strategy='random')
          [output] = sess.run([features_op], feed_dict={rgb_input: batch_video})
          features = np.concatenate((features, output), axis=0)

          # if counter % 5000 == 0:
          #   np.save(os.path.join(experiments_dir, 'predictions', suffix + str(num_prova), 'X_' + split + '_' + str(counter).zfill(2)), features)

          counter += 1

        # Save features as output of the network
        np.save(os.path.join(experiments_dir, 'predictions', suffix + str(num_prova), 'X_' + split + '_mixed_5c_' + str(repeat).zfill(2)), features)


if __name__ == "__main__":
  main()