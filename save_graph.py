import tensorflow as tf
from i3d import InceptionI3d

config = tf.ConfigProto()
config.gpu_options.allocator_type = 'BFC'
config.gpu_options.per_process_gpu_memory_fraction = 0.95
config.gpu_options.allow_growth = True


if __name__ == '__main__':
  """
  Use this script to just save graph and checkpoint.
  While training, checkpoints are saved. You can test them with this python code.
  """

  num_prova = 14
  num_frames_per_clip = 16
  crop_size = 224
  num_classes = 60

  i3d_input = tf.placeholder(tf.float32, shape=(None, num_frames_per_clip, crop_size, crop_size, 3), name='video')

  with tf.variable_scope('RGB'):
    rgb_model = InceptionI3d(num_classes=num_classes, spatial_squeeze=True, final_endpoint='Logits')
  logits, _ = rgb_model(i3d_input, is_training=False, dropout_keep_prob=1.0)
  probabilities = tf.nn.softmax(logits, name='probabilities')

  fine_tuning_var_map = {}
  restore_weights_map = {}
  for variable in tf.global_variables():
    if variable.name.split('/')[2] == 'Logits' or \
      variable.name.split('/')[2] == 'Mixed_5c' or \
      variable.name.split('/')[2] == 'Mixed_5b':
      fine_tuning_var_map[variable.name.replace(':0', '')] = variable
    if variable.name.split('/')[2] != 'Logits':
      restore_weights_map[variable.name.replace(':0', '')] = variable
  rgb_saver = tf.train.Saver(var_list=restore_weights_map, reshape=True)
  classification_saver = tf.train.Saver(var_list=fine_tuning_var_map)

  with tf.Session(config=config) as sess:

    # Recover last checkpoints (if any) for classification filters, moving averages, ...
    last_checkpoint_file = tf.train.latest_checkpoint(checkpoint_dir='/home/alessio/Sandbox/experiments/NTU-RGBD/checkpoints/PROVA_' + str(num_prova),
                                                      latest_filename='I3D_finetuning_rgb_' + str(num_prova) + '_latest')

    if last_checkpoint_file != None:
      last_step = last_checkpoint_file.split('-')[-1]
      print('Found the following checkpoint:', last_checkpoint_file)
      print('Last iteration was', last_step)

    # Run the initializer
    sess.run(tf.global_variables_initializer())

    # Restore al trained model and last checkpoints (if any)
    rgb_saver.restore(sess, '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/rgb_imagenet/model.ckpt')
    print('RGB branch variables successfully restored.')
    if last_checkpoint_file != None:
      classification_saver.restore(sess, last_checkpoint_file)
      print('Classification variables successfully restored.')


    tf.train.write_graph(sess.graph_def, '/tmp', 'i3d-graph.pbtxt', as_text=True)

    graph = tf.get_default_graph()
    dir(graph)

    saver = tf.train.Saver()
    saver.save(sess, '/tmp/chk', global_step=int(last_step))
