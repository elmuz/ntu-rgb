from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from i3d import InceptionI3d


def get_params():
  """Return model parameters."""
  return {
    'input_size': 224,
    'num_frames_per_clip': 32,
    'dropout_keep_prob': 0.7,
    'init_weights' :'/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/rgb_imagenet/model_v2.ckpt',
  }



def make_I3D(features, labels, mode, params, config=None):

  with tf.variable_scope('RGB'):
    rgb_model = InceptionI3d(num_classes=params.num_classes, spatial_squeeze=True, final_endpoint='Logits')
  logits, _ = rgb_model(features, is_training=False, dropout_keep_prob=params.dropout_keep_prob)

  if mode == tf.estimator.ModeKeys.TRAIN:
    restore_weights_map = {}
    for variable in tf.global_variables():
      if variable.name.startswith('RGB') and 'Logits' not in variable.name.split('/'):
        restore_weights_map[variable.name.replace(':0', '')] = variable
    tf.train.init_from_checkpoint(params.init_weights, restore_weights_map)

  probabilities = tf.nn.softmax(logits)

  # Define loss
  if (mode == tf.estimator.ModeKeys.TRAIN or mode == tf.estimator.ModeKeys.EVAL):
    loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels))
  else:
    loss_op = None

  # Evaluation metrics
  predictions = tf.argmax(probabilities, axis=-1)
  eval_metrics = {
    "accuracy": tf.metrics.accuracy(labels, predictions)
  }

  # Summaries
  tf.summary.scalar('loss', loss_op)
  #merged_summary = tf.summary.merge_all()

  # Define optimizer
  global_step = tf.train.get_or_create_global_step()
  if mode == tf.estimator.ModeKeys.TRAIN:
    learning_rate = tf.train.exponential_decay(params.initial_learning_rate,
                                               global_step,
                                               params.decay_steps,
                                               params.decay_rate,
                                               staircase=False)
    optimizer = tf.train.AdamOptimizer(learning_rate)

    # Define exponential moving average for all trained variables
    print('Training variable selection')
    fine_tuning_var_map = {}
    for variable in tf.global_variables():
      if variable.name.startswith('RGB'):
        if variable.name.split('/')[2] == 'Logits' or \
            variable.name.split('/')[2] == 'Mixed_5c' or \
            variable.name.split('/')[2] == 'Mixed_5b' or \
            variable.name.split('/')[2] == 'Mixed_4f':
          fine_tuning_var_map[variable.name.replace(':0', '')] = variable

    exp_moving_avg = tf.train.ExponentialMovingAverage(0.99)
    variables_averages_op = exp_moving_avg.apply(list(fine_tuning_var_map.values()))

    shadow_ema_vars = {}
    for var in fine_tuning_var_map.values():
      shadow_ema_vars[exp_moving_avg.average_name(var)] = exp_moving_avg.average(var)
    opt_op = optimizer.minimize(loss_op, global_step=global_step, var_list=list(fine_tuning_var_map.values()))

    with tf.control_dependencies([opt_op]):
      train_op = tf.group(variables_averages_op)
  else:
    train_op = None

  return tf.estimator.EstimatorSpec(
        mode=mode,
        predictions={"predictions": predictions},
        loss=loss_op,
        train_op=train_op,
        eval_metric_ops=eval_metrics)
