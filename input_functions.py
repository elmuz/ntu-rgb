import os
import numpy as np
import tensorflow as tf
import cv2


DATA_DIR = '/home/alessio/Sandbox/NTU'
NUM_CLASSES = 60

def get_params():
  """Return dataset parameters."""
  return {
    'dataset_path': DATA_DIR,
    'num_classes': NUM_CLASSES,
  }


def pack_frames(filename,
                label,
                length,
                n_frames,
                strategy='random',
                s_frame=0,
                hop_size=8):

  # Randomly sample 'num_frames' consecutive frames
  filename = filename.decode('ascii')
  if strategy == 'random':
    starting_frame = np.squeeze(np.random.choice(max(0, length - n_frames) + 1, 1))
    next_starting_frame = 0
  elif strategy == 'progressive':
    starting_frame = s_frame  # it already should have been checked that there are enough frames
    next_starting_frame = starting_frame + hop_size
    # If the next videocube can have at least n_frames/2 good frames take it
    # (and pad if needed), otherwise skip to the next video.
    if next_starting_frame +  (n_frames // 2) >= length:
      next_starting_frame = -1
  else:  # take the center clip
    starting_frame = max(0, (length - n_frames) // 2)
    next_starting_frame = 0
  video_cube = np.empty((0, 384, 512, 3), dtype=np.uint8)
  for f in range(starting_frame, min(length, starting_frame + n_frames)):
    frame = cv2.imread(os.path.join(DATA_DIR, 'frames', filename, str(f).zfill(4) + '.jpg'), cv2.IMREAD_COLOR)
    video_cube = np.concatenate((video_cube, [frame]), axis=0)
  if np.shape(video_cube)[0] < n_frames:
    # Pad with last frame
    video_cube = np.concatenate((video_cube, np.tile([video_cube[-1]], [n_frames - np.shape(video_cube)[0], 1, 1, 1])))

  return video_cube, label, length, next_starting_frame


def preprocessing(mode, img_size, video_cube, label, length):

  if mode == tf.estimator.ModeKeys.TRAIN:

    # Final size should be 224x224. Input size should be 512x384.
    # 5 possible crops are considered, plus "no-crop" case. Crops can be at the
    # corners or centered, with ~20% reduction.
    # 512x384 -> 320x320 -> 224x224

    crop_boxes = tf.constant([[0, 0, 320 / 384, 320 / 512], # NW
                            [1 - 320 / 384, 0, 1, 320 / 512], # SW
                            [0, 1 - 320 / 512, 320 / 384, 1], # NE
                            [1 - 320 / 384, 1 - 320 / 512, 1, 1], # SE
                            [0.5 - 160 / 384, 0.5 - 160 / 512, 0.5 + 160 / 384, 0.5 + 160 / 512], # C
                            [0, 0.5 - 192 / 512, 1, 0.5 + 192 / 512], # C
                            [0, 320 / 512, 320 / 384, 0], # NW flipped
                            [1 - 320 / 384, 320 / 512, 1, 0], # SW flipped
                            [0, 1, 320 / 384, 1 - 320 / 512], # NE flipped
                            [1 - 320 / 384, 1, 1, 1 - 320 / 512], # SE flipped
                            [0.5 - 160 / 384, 0.5 + 160 / 512, 0.5 + 160 / 384, 0.5 - 160 / 512], # C flipped
                            [0, 0.5 + 192 / 512, 1, 0.5 - 192 / 512]], # C flipped
                            dtype=tf.float32)

    crop_idx = tf.squeeze(tf.random_uniform([1], minval=0, maxval=tf.shape(crop_boxes)[0], dtype=tf.int32))
    boxes = tf.tile(tf.expand_dims(crop_boxes[crop_idx, :], axis=0), tf.stack([tf.shape(video_cube)[0], 1]))

  else:

    # Final size should be 224x224. Input size should be 512x384.
    # 512x384 -> 320x320 -> 224x224

    crop_boxes = tf.constant([0, 0.5 - 192 / 512, 1, 0.5 + 192 / 512], dtype=tf.float32)
    boxes = tf.tile(tf.expand_dims(crop_boxes, axis=0), tf.stack([tf.shape(video_cube)[0], 1]))

  video_cube_crop = tf.image.crop_and_resize(video_cube,
                                             boxes,
                                             tf.range(tf.shape(video_cube)[0], dtype=tf.int32),
                                             tf.constant([img_size, img_size], dtype=tf.int32))

  video_cube_crop.set_shape(tf.TensorShape([None, 224, 224, 3]))  # assert 3 channel data

  # Normalize [-1, +1]
  video_cube_crop /= 127.
  video_cube_crop -= 1.

  return video_cube_crop, label, length


def get_skel(video_cube, pose_estimator):
  num_frames, image_h, image_w, _ = np.shape(video_cube)
  min_x = min_y = np.inf
  max_x = max_y = 0
  for i in range(num_frames):
    humans = pose_estimator.inference(video_cube[i])
    for human in humans:
      # Find origin as the mean of all joints
      for i in range(18):
        if i not in human.body_parts.keys():
          continue

        body_part = human.body_parts[i]
        if body_part.x * image_w > max_x:
          max_x = body_part.x * image_w
        if body_part.x * image_w < min_x:
          min_x = body_part.x * image_w
        if body_part.y * image_h > max_y:
          max_y = body_part.y * image_h
        if body_part.y * image_h < min_y:
          min_y = body_part.y * image_h


def get_box(video_cube, pose_estimator):
  num_frames, image_h, image_w, _ = np.shape(video_cube)
  min_x = min_y = np.inf
  max_x = max_y = 0
  subject_height = 0
  # TODO:
  # If more than one subject is found limit the search to the "main one"
  # Consider a very simple tracking heuristics. If more than on
  for i in range(num_frames):
    humans = pose_estimator.inference(video_cube[i])
    for human in humans:
      for i in range(18):
        if i not in human.body_parts.keys():
          continue

        body_part = human.body_parts[i]
        if body_part.x * image_w > max_x:
          max_x = body_part.x * image_w
        if body_part.x * image_w < min_x:
          min_x = body_part.x * image_w
        if body_part.y * image_h > max_y:
          max_y = body_part.y * image_h
        if body_part.y * image_h < min_y:
          min_y = body_part.y * image_h

      # Estimate the scale considering measure of known limbs (from 0th to 11th)
      limbs = [(1, 2), (1, 5), (2, 3), (3, 4), (5, 6), (6, 7), (1, 8), (8, 9), (9, 10), (1, 11), (11, 12), (12, 13)]
      limbs_ratio = [0.093, 0.093, 0.153, 0.113, 0.153, 0.113, 0.296, 0.211, 0.224, 0.296, 0.211, 0.224]
      estimated_height = []
      for l in range(len(limbs)):  # check main 12 limbs
        if limbs[l][0] not in human.body_parts.keys() or limbs[l][1] not in human.body_parts.keys():
          continue
        length_temp =  np.sqrt((human.body_parts[limbs[l][0]].x - human.body_parts[limbs[l][1]].x)**2 + (human.body_parts[limbs[l][0]].y - human.body_parts[limbs[l][1]].y)**2)
        estimated_height.append(length_temp * image_h / limbs_ratio[l])

        # average the height estimated from any limb
        avg_height = np.mean(estimated_height)
        if avg_height > subject_height:
          subject_height = avg_height

  # check if pose-estimation failed
  if np.isinf(min_y) or np.isinf(min_x) or max_y == 0 or max_x == 0:  # this means it failed getting the pose for every frame
    return [0, 0, image_h, image_w]

  # Adjust the min_y max_y according to the estimated height
  h_temp = max_y - min_y
  if subject_height > h_temp:
    min_y -= (subject_height - h_temp) / 2
    max_y += (subject_height - h_temp) / 2

  # First add a random extra padding, then check whether
  # it is still contained in the image
  h_temp = max_y - min_y
  w_temp = max_x - min_x
  padding = np.random.rand(4) * 0.2  # padding between [0., 0.2)

  min_x = int(max(0., min_x - (w_temp * padding[0])))
  min_y = int(max(0., min_y - (h_temp * padding[1])))
  max_x = int(min(image_w - 1., max_x + (w_temp * padding[2])))
  max_y = int(min((image_h - 1., max_y + (h_temp * padding[3]))))

  crop_size = max(max_x - min_x, max_y - min_y)  # choose the biggest dimension for cropping

  if crop_size > image_h:  # horizontal patch, too wide compared to height
    crop_size = image_h
    # Adjust min_x and max_x accordingly
    offset = np.random.randint(0, (max_x - min_x) - crop_size + 1)
    min_x += offset
    max_x = min_x + crop_size
  if crop_size > image_w:  # vertical patch, too high compared to width
    crop_size = image_w
    # Adjust min_y and max_y accordingly
    offset = np.random.randint(0, (max_y - min_y) - crop_size + 1)
    min_y += offset
    max_y = min_y + crop_size


  delta = abs((max_x - min_x) - (max_y - min_y))
  if (max_x - min_x) - (max_y - min_y) < 0:  # portrait -> random horizontal crop
    offset_min = max(0, min_x - delta)
    offset_max = min(min_x, image_w - delta - (max_x - min_x))
    if offset_min > offset_max:
      print('Portrait')
      print('min_y', min_y, 'min_x', min_x, 'max_y', max_y, 'max_x', max_x)
      print('crop_size', crop_size, 'delta', delta)
    offset = np.random.randint(offset_min, offset_max + 1)

    return [min_y, offset, max_y, offset + crop_size]

  else:  # landscape
    offset_min = max(0, min_y - delta)
    offset_max = min(min_y, image_h - delta - (max_y - min_y))
    if offset_min > offset_max:
      print('Landscape')
      print('min_y', min_y, 'min_x', min_x, 'max_y', max_y, 'max_x', max_x)
      print('crop_size', crop_size, 'delta', delta)
    offset = np.random.randint(offset_min, offset_max + 1)

    return [offset, min_x, offset + crop_size, max_x]



def make_input_fn(mode, params, num_threads=8):

  def _parse(*args):
    return preprocessing(mode, params.input_size, *args)

  def _input_fn():

    # .npy data are in the form of [filename(string), label(int), video-length(int)]
    if mode == tf.estimator.ModeKeys.TRAIN:
      np_data = np.load(os.path.join(params.dataset_path, 'lists', 'training_list.npy'))
    else:
      np_data = np.load(os.path.join(params.dataset_path, 'lists', 'testing_list.npy'))

    #augmentation = False

    filenames = tf.constant(np_data['file'])
    labels = tf.cast(tf.constant(np_data['label']), dtype=tf.int32)
    lengths = tf.constant(np_data['frames'])

    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):
      dataset = tf.data.Dataset.from_tensor_slices((filenames, labels, lengths))

      if mode == tf.estimator.ModeKeys.TRAIN:
        dataset = dataset.repeat()
        dataset = dataset.shuffle(buffer_size=np.shape(np_data)[0])

      dataset = dataset.map(lambda filename, label, length: tuple(tf.py_func(pack_frames,
                                                                             [filename, label, length, params.num_frames_per_clip],
                                                                             [tf.uint8, label.dtype, length.dtype])),
                            num_parallel_calls=4)

      dataset = dataset.map(_parse, num_parallel_calls=num_threads)
      dataset = dataset.batch(params.batch_size)
      dataset = dataset.prefetch(num_threads // 2)

      iterator = dataset.make_initializable_iterator()
      tf.add_to_collection(tf.GraphKeys.TABLE_INITIALIZERS, iterator.initializer)
      next_element, next_label, _ = iterator.get_next()

      return next_element, next_label

  return _input_fn

if __name__ == '__main__':
  exit(0)