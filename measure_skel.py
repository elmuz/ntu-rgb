import os
import numpy as np
import cv2

limbs  = [(1, 2), (1, 5), (2, 3), (3, 4), (5, 6), (6, 7), (1, 8), (8, 9), (9, 10), (1, 11), (11, 12), (12, 13), (1, 0), (0, 14), (14, 16), (0, 15), (15, 17), (2, 16), (5, 17)]

joints = np.array([2.15438248e+02, 5.53403511e+01, 8.54443431e-01,
            2.18519150e+02, 1.19698776e+02, 8.73201132e-01,
            1.65249451e+02, 1.18166046e+02, 8.31807971e-01,
            1.44867401e+02, 1.98176224e+02, 7.97929764e-01,
            1.58949341e+02, 2.57856140e+02, 7.81617761e-01,
            2.68788757e+02, 1.19728348e+02, 8.29004705e-01,
            2.93858124e+02, 2.02871048e+02, 8.12397480e-01,
            2.60975769e+02, 2.57810852e+02, 7.52781272e-01,
            1.80899567e+02, 2.81371857e+02, 6.39129281e-01,
            1.71579666e+02, 3.97499878e+02, 6.75245285e-01,
            1.62106781e+02, 5.24538330e+02, 7.09473848e-01,
            2.43720566e+02, 2.79806305e+02, 6.32066548e-01,
            2.46855698e+02, 3.97464722e+02, 6.59688354e-01,
            2.40550125e+02, 5.18289612e+02, 6.84564888e-01,
            2.05954422e+02, 4.44374657e+01, 9.15107906e-01,
            2.23312378e+02, 4.44430580e+01, 8.98463845e-01,
            1.91900711e+02, 5.69416809e+01, 9.32958364e-01,
            2.40568008e+02, 5.69166260e+01, 9.37182426e-01 ])

joints = np.reshape(joints, (18, 3))

h = 555
lengths = []  # limbs length, once total height is fixed to 1.0

img = cv2.imread('/home/alessio/Scaricati/man.jpg')
for j in range(18):
  cv2.circle(img, (int(joints[j, 0]), int(joints[j, 1])), 5, (0, 0, 255), -1)
for l in range(len(limbs)):
  cv2.putText(img, str(l), (int(joints[limbs[l][0], 0] + joints[limbs[l][1], 0]) // 2, int(joints[limbs[l][0], 1] + joints[limbs[l][1], 1]) // 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
  cv2.line(img, (int(joints[limbs[l][0], 0]), int(joints[limbs[l][0], 1])), (int(joints[limbs[l][1], 0]), int(joints[limbs[l][1], 1])), (0, 255, 0), 2)
  length = np.sqrt((joints[limbs[l][0], 0] - joints[limbs[l][1], 0])**2 + (joints[limbs[l][0], 1] - joints[limbs[l][1], 1])**2) / h
  print(length)
  lengths.append(length)
lengths[0] = 0.5 * (lengths[0] + lengths[1])
lengths[1] = lengths[0]
lengths[2] = 0.5 * (lengths[2] + lengths[4])
lengths[4] = lengths[2]
lengths[3] = 0.5 * (lengths[3] + lengths[5])
lengths[5] = lengths[3]
lengths[6] = 0.5 * (lengths[6] + lengths[9])
lengths[9] = lengths[6]
lengths[7] = 0.5 * (lengths[10] + lengths[7])
lengths[10] = lengths[7]
lengths[8] = 0.5 * (lengths[8] + lengths[11])
lengths[11] = lengths[8]

print(lengths)

cv2.imshow('', img)
cv2.waitKey(0)