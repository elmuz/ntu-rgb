from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import time

import numpy as np
import cv2
from PIL.Image import blend

import tracking
from preprocessing import CircularBuffer, Predictor
from estimator import TfPoseEstimator
from action_estimator import TfActionEstimator
from object_estimator import  TfObjectEstimator

def main():

  # Dataset
  dataset = 'NTU-RGBD'
  experiments_dir = os.path.join('/home/alessio/Sandbox/experiments', dataset)
  classes_file = '/home/alessio/Sandbox/NTU/classes.csv'
  classes_file_short = '/home/alessio/Sandbox/NTU/classes_short.csv'
  classes = np.genfromtxt(fname=classes_file, dtype=str, delimiter=',', usecols=[0])
  num_classes = 60

  class_mapping = {
    0: 0, 1: 0, 2: 1, 3: 1, 4: 2, 5: 2, 6: 2, 7: 3, 8: 4, 9: 20, 10: 5, 11: 5, 12: 20,
    13: 6, 14: 6, 15: 7, 16: 7, 17: 8, 18: 8, 19: 20, 20: 20, 21: 20, 22: 9, 23: 20,
    24: 20, 25: 20, 26: 20, 27: 10, 28: 20, 29: 20, 30: 11, 31: 20, 32: 20, 33: 20,
    34: 20, 35: 20, 36: 1, 37: 20, 38: 20, 39: 20, 40: 12, 41: 13, 42: 14, 43: 15,
    44: 16, 45: 17, 46: 18, 47: 19, 48: 20, 49: 20, 50: 20, 51: 20, 52: 20, 53: 20,
    54: 20, 55: 20, 56: 20, 57: 20, 58: 20, 59: 20,
  }

  # Model hyperparameters
  num_frames_per_clip = 32
  crop_size = 224
  img_channels = 3
  threshold = 0.5

  # Prepare the model
  if num_frames_per_clip == 16:
    action_estimator = TfActionEstimator('/home/alessio/Sandbox/dev/NTU-RGBD/models/i3d-16-70325.pb', target_size=(num_frames_per_clip, crop_size, crop_size))
  else:
    action_estimator = TfActionEstimator('/home/alessio/Sandbox/dev/NTU-RGBD/models/i3d-32-23475.pb', target_size=(32, crop_size, crop_size))
  pose_estimator = TfPoseEstimator('/home/alessio/Sandbox/dev/OpenPose-TensorFlow/models/graph/mobilenet_thin/graph_opt.pb', target_size=(432, 368))
  object_detector = TfObjectEstimator('/home/alessio/Scaricati/ssd_mobilenet_v1_coco_2017_11_17/frozen_inference_graph.pb')

  # Prepare input
  webcam_reader = CircularBuffer(src=0, num_frames_per_clip=num_frames_per_clip).start()
  [cam_width, cam_height, cam_fps] = webcam_reader.get_specs()
  print('Webcam specs: ' + str(int(cam_width)) + 'x' + str(int(cam_height)) + ' @ ' + str((cam_fps)) + 'fps')

  # Prepare output
  # video_writer = cv2.VideoWriter('/tmp/actions_webcam.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), cam_fps, (cam_width, cam_height))

  tracking.Track.image_w = cam_width
  tracking.Track.image_h = cam_height

  counter = 0
  pose_interval = 3
  action_interval = 5
  T = 1 / cam_fps  # desired interval between iterations

  fps_win = 30  # average fps over a window
  fps = 1 / 30 * np.ones(fps_win, dtype=np.float32)

  tracks = tracking.TrackList()
  workers = {}
  humans = []

  while True:

    t1 = time.time()

    frame = np.copy(webcam_reader.get_frame())

    # if counter % pose_interval == 0:
      # frame2 = cv2.resize(frame[:, :, [2, 1, 0]], (300, 300))
      # tx = time.time()
      # objs = object_detector.inference(frame)
      # print(time.time() - tx)
      # for i in range(int(objs[0][0])):
      #   if float(objs[1][0][i]) > 0.3 and int(objs[3][0][i]) == 1:
      #     pass

    if counter % pose_interval == 0:

      # TODO: convert to object detection network instead of pose estimation
      humans = pose_estimator.inference(frame[:, :, :])
      tracks.update_tracks(humans, frame)

    frame = TfPoseEstimator.draw_humans(frame, humans, blend=True)
      # scores = []
      # for human in humans:
      #   max_score = 0
      #   for joint in range(18):
      #     if joint not in human.body_parts.keys():
      #       continue
      #     if human.body_parts[joint].score > max_score:
      #       max_score = human.body_parts[joint].score
      #   scores.append(max_score)
      # print(len(humans), 'Max scores:', scores)

    # for t in tracks.keys():
    #   cv2.rectangle(frame,
    #                 (tracks[t].crop_box[1], tracks[t].crop_box[0]), (tracks[t].crop_box[3], tracks[t].crop_box[2]),
    #                 (int(tracks[t].box_color[0]), int(tracks[t].box_color[1]), int(tracks[t].box_color[2])),
    #                 3)
    #   cv2.rectangle(frame,
    #                 (int(tracks[t].box[-1, 1]), int(tracks[t].box[-1, 0])), (int(tracks[t].box[-1, 3]), int(tracks[t].box[-1, 2])),
    #                 (0, 255, 0),
    #                 1)

    if counter % action_interval == 0:

      for t in tracks.keys():
        workers[t] = Predictor(action_estimator, tracks[t], webcam_reader.get_cube()).start()

    frame = cv2.flip(frame, 1)

    for t in tracks.keys():
      center = np.mean(tracks[t].center, axis=0).astype(np.int32)
      action = np.argmax(np.mean(tracks[t].actions, axis=0))
      score = np.amax(np.mean(tracks[t].actions, axis=0))
      if score > threshold:
        cv2.putText(frame,
                    classes[action],
                    (cam_width - center[1], center[0]),
                    cv2.FONT_HERSHEY_SIMPLEX, .8, (0, 0, 255), 2, cv2.LINE_AA)
        # cv2.putText(frame,
        #             str(tracks[t].age),
        #             (cam_width - int(tracks[t].box[-1, 3]) + 3, int(tracks[t].box[-1, 0]) + 15),
        #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (100, 0, 0), 1)

    # if counter == 0:
    #   print(time.time() - t1)
    wait_time = (T - (time.time() - t1))
    if wait_time > 0:
      time.sleep(wait_time)

    fps = np.concatenate((fps[1:], [time.time() - t1]), axis=0)
    cv2.putText(frame, 'FPS: ' + "{0:.2f}".format(1 / (np.mean(fps))), (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    cv2.imshow('tf-pose-estimation result', frame)
    # video_writer.write(frame)
    if cv2.waitKey(1) == 27:
      break

    counter += 1


if __name__ == "__main__":
  main()