from threading import Thread
import numpy as np
import cv2


class CircularBuffer:

  img_channels = 3

  def __init__(self, src=0, num_frames_per_clip=32):
    # initialize the video camera stream and read the first frame
    # from the stream
    self.stream = cv2.VideoCapture(src)
    # self.stream = cv2.VideoCapture('/home/alessio/Scaricati/people_walking.mp4')
    self.frame_width = int(self.stream.get(3))
    self.frame_height = int(self.stream.get(4))
    self.fps = self.stream.get(5)
    self.num_frames_clip = num_frames_per_clip
    self.webcam_cube = np.zeros((self.num_frames_clip, self.frame_height, self.frame_width, self.img_channels), dtype=np.uint8)
    self.small_cube = np.zeros((self.num_frames_clip, 224, 224, 3), dtype=np.uint8)
    (self.grabbed, self.frame) = self.stream.read()

    # initialize the variable used to indicate if the thread should be stopped
    self.stopped = False

  def get_specs(self):
    return [self.frame_width, self.frame_height, self.fps]

  def start(self):
    # start the thread to read frames from the video stream
    Thread(target=self.update, args=()).start()
    return self

  def update(self):
    # keep looping infinitely until the thread is stopped

    while True:

      # if the thread indicator variable is set, stop the thread
      if self.stopped:
        return

      # otherwise, read the next frame from the stream
      (self.grabbed, self.frame) = self.stream.read()
      self.webcam_cube = np.concatenate((self.webcam_cube[1:], [self.frame]), axis=0)
      self.small_cube = np.concatenate((self.small_cube[1:], [cv2.resize(self.frame, (224, 224), interpolation=cv2.INTER_AREA)]), axis=0)


  def get_frame(self):
    # return the frame most recently read
    return self.frame

  def get_cube(self):
    # return the frame most recent cube
    return self.webcam_cube

  def get_small_cube(self):
    # return the frame most recent cube
    return self.small_cube

  def stop(self):
    # indicate that the thread should be stopped
    self.stopped = True


class Predictor:

  def __init__(self, estimator, track, webcam_cube):
    self.estimator = estimator
    self.track = track
    self.crop_cube = np.copy(webcam_cube[:, track.crop_box[0]:track.crop_box[2], track.crop_box[1]:track.crop_box[3], :])

  def start(self):

    Thread(target=self.predict, args=()).start()
    return self

  def predict(self):
    input_cube = np.empty((0, 224, 224, 3), dtype=np.float32)
    for f in range(np.shape(self.crop_cube)[0]):
      input_cube = np.concatenate((input_cube, [cv2.resize(self.crop_cube[f], (224, 224) , interpolation=cv2.INTER_LINEAR)]))
    input_cube = input_cube.astype(np.float32) / 127. - 1.
    probabilities = self.estimator.inference([input_cube])
    self.track.actions = np.concatenate((self.track.actions[1:, :], probabilities), axis=0)

