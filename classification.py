import os
import numpy as np
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

# Dataset parameters
dataset = 'NTU'
dataset_path = os.path.join('/home/alessio/Sandbox', dataset)
classes_file = '/home/alessio/Sandbox/NTU/classes.csv'
classes = np.genfromtxt(fname=classes_file, dtype=str, delimiter=',', usecols=0)
num_samples = 5040
num_frames_per_clip = 32
num_classes = 60
num_splits = 1
experiments_dir = os.path.join('/home/alessio/Sandbox/experiments', dataset + '-RGBD')
num_samples_per_split = num_samples

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.jet):
  """
  This function prints and plots the confusion matrix.
  Normalization can be applied by setting `normalize=True`.
  """

  if normalize:
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print("Normalized confusion matrix")
  else:
    print('Confusion matrix, without normalization')

  print(cm)

  plt.imshow(cm, interpolation='nearest', cmap=cmap)
  plt.title(title)

  plt.colorbar()

  tick_marks = np.arange(len(classes))
  plt.xticks(tick_marks, classes, rotation=90, fontsize=6)
  plt.yticks(tick_marks, classes, fontsize=6)

  plt.tight_layout()
  plt.ylabel('True label', fontsize=12)
  plt.xlabel('Predicted label', fontsize=12)

  plt.show()


def threshold_clips(predictions, labels, IDs, threshold):

  clip_predictions = np.empty((0, num_classes), dtype=np.float32)
  clip_labels = []
  clip_IDs = []

  # Filter those clips with one dominant probability
  for clip in range(np.shape(predictions)[0]):
    probabilities_temp = predictions[clip].astype(np.float32)
    max_prob = np.amax(probabilities_temp).astype(np.float32)
    max_pred = np.argmax(probabilities_temp)
    probabilities_temp[max_pred] = 0.0
    second_prob = np.amax(probabilities_temp).astype(np.float32)
    if max_prob / second_prob > threshold:
      clip_predictions = np.concatenate((clip_predictions, [predictions[clip]]), axis=0)
      clip_labels.append(labels[clip])
      clip_IDs.append(IDs[clip])

  return clip_predictions, clip_labels, clip_IDs


def clip_based_confusion(predictions, labels):

  # Assign each clip to its highest probability, regardless which video they belong to
  cnf = confusion_matrix(y_true=np.array(labels), y_pred=np.argmax(predictions, axis=1))
  cnf = cnf.astype('float') / cnf.sum(axis=1)[:, np.newaxis]
  accuracy = np.mean(np.diag(cnf))
  plot_confusion_matrix(cnf, classes, normalize=True, title='Clip-Based. Accuracy: ' + str(accuracy))


def video_based_confusion(predictions, labels, IDs, strategy='sum'):
  # Sum each clip's probabilities for every video they belong to. Take highest score for each video.

  video_predictions = np.empty((0), dtype=np.int16)
  video_labels = []

  if strategy == 'sum':
    last_ID = 0
    clip = 0
    video_temp_predictions = np.zeros((num_classes))
    for clip in range(np.shape(IDs)[0]):
      if IDs[clip] == last_ID:
        video_temp_predictions += predictions[clip]
      else:
        video_predictions = np.concatenate((video_predictions, [np.argmax(video_temp_predictions)]))
        video_temp_predictions = np.zeros((num_classes))
        video_temp_predictions += predictions[clip]
        video_labels.append(labels[clip - 1])
        last_ID = IDs[clip].astype(np.int32)
    video_predictions = np.concatenate((video_predictions, [np.argmax(video_temp_predictions)]))
    video_labels.append(labels[clip])

  elif strategy == 'highest':
    last_ID = 0
    clip = 0
    highest_temp = 0
    video_temp_prediction = -1
    for clip in range(np.shape(IDs)[0]):
      if IDs[clip] == last_ID:
        if np.amax(predictions[clip]) > highest_temp:
          highest_temp = np.amax(predictions[clip])
          video_temp_prediction = np.argmax(predictions[clip])
      else:
        video_predictions = np.concatenate((video_predictions, [video_temp_prediction]))
        highest_temp = np.amax(predictions[clip])
        video_temp_prediction = np.argmax(predictions[clip])
        video_labels.append(labels[clip - 1])
        last_ID = IDs[clip]
    video_predictions = np.concatenate(
        (video_predictions, [video_temp_prediction]))
    video_labels.append(labels[clip])

  elif strategy == 'majority':
    last_ID = 0
    clip = 0
    video_temp_predictions = np.zeros((num_classes))
    for clip in range(np.shape(IDs)[0]):
      if IDs[clip] == last_ID:
        video_temp_predictions[np.argmax(predictions[clip])] += 1
      else:
        video_predictions = np.concatenate((video_predictions, [np.argmax(video_temp_predictions)]))
        video_temp_predictions = np.zeros((num_classes))
        video_temp_predictions[np.argmax(predictions[clip])] += 1
        video_labels.append(labels[clip - 1])
        last_ID = IDs[clip].astype(np.int32)
    video_predictions = np.concatenate((video_predictions, [np.argmax(video_temp_predictions)]))
    video_labels.append(labels[clip])

  cnf = confusion_matrix(y_true=video_labels, y_pred=video_predictions)
  cnf = cnf.astype('float') / cnf.sum(axis=1)[:, np.newaxis]
  accuracy = np.mean(np.diag(cnf))
  plot_confusion_matrix(cnf, classes, normalize=True, title='Video-Based. Aggreg: ' + strategy + '. Accuracy: ' + str(accuracy))


predictions = np.empty((0, num_classes), dtype=np.float32)
labels = np.array([], dtype=np.int32)
IDs = np.array([], dtype=np.int64)

num_splits = 1
for split in range(1, num_splits + 1):
  # for split in [1]:

  # predictions_temp = np.load(os.path.join(experiments_dir, 'predictions', 'split' + suffix + str(split), 'I3D_masked_predictions_' + str(num_frames_per_clip) + '.npy'))
  # labels_temp = np.load(os.path.join(experiments_dir, 'predictions', 'split' + suffix + str(split), 'I3D_labels_' + str(num_frames_per_clip) + '.npy'))
  # IDs_temp = np.load(os.path.join(experiments_dir, 'predictions', 'split' + suffix + str(split), 'I3D_IDs_' + str(num_frames_per_clip) + '.npy'))
  predictions_temp = np.load(os.path.join(experiments_dir, 'predictions', 'PROVA_8', 'I3D_predictions_' + str(num_frames_per_clip) + '.npy'))
  labels_temp = np.load(os.path.join(experiments_dir, 'predictions', 'PROVA_8', 'I3D_labels_' + str(num_frames_per_clip) + '.npy'))
  IDs_temp = np.load(os.path.join(experiments_dir, 'predictions', 'PROVA_8', 'I3D_IDs_' + str(num_frames_per_clip) + '.npy'))

  predictions = np.concatenate((predictions, predictions_temp), axis=0)
  labels = np.concatenate((labels, labels_temp), axis=0)
  IDs = np.concatenate((IDs, IDs_temp + num_samples_per_split * (split - 1)), axis=0)

clip_predictions, clip_labels, clip_IDs = threshold_clips(predictions, labels, IDs, 1.0)
for i in range(num_samples):
 if i not in clip_IDs:
   print(i)
#clip_based_confusion(clip_predictions, clip_labels)
video_based_confusion(clip_predictions, clip_labels, clip_IDs, strategy='sum')
