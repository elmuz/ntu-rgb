from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import shutil
from time import time, gmtime, strftime
import numpy as np
import tensorflow as tf
from input_functions import pack_frames, get_box
from i3d import InceptionI3d
from estimator import TfPoseEstimator
from networks import get_graph_path

import cv2

def main():
  _CHECKPOINT_PATHS = {
    'rgb': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/rgb_scratch/model.ckpt',
    'flow': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/flow_scratch/model.ckpt',
    'rgb_imagenet': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/rgb_imagenet/model.ckpt',
    'flow_imagenet': '/home/alessio/Sandbox/dev/kinetics-i3d/data/checkpoints/flow_imagenet/model.ckpt',
  }

  # Dataset
  dataset = 'NTU-RGBD'
  experiments_dir = os.path.join('/home/alessio/Sandbox/experiments', dataset)
  classes_file = '/home/alessio/Sandbox/NTU/classes.csv'
  classes = np.genfromtxt(fname=classes_file, dtype=str, delimiter=',', usecols=[0])
  num_classes = 59
  num_frames_per_clip = 32
  crop_size = 224
  img_channels = 3

  # PROVA
  num_prova = 15
  suffix = 'PROVA_'

  # Training parameters
  training_steps = 5e3
  final_step = 12e4
  initial_learning_rate = 2e-4
  batch_size = 10
  dropout_keep_prob = 0.7
  display_steps = 25
  checkpoint_steps = 25

  # Change directory according to OpenPose-TensorFlow
  os.chdir('/home/alessio/Sandbox/dev/OpenPose-TensorFlow')

  # Prepare the model
  pose_estimator = TfPoseEstimator(get_graph_path('mobilenet_thin'), target_size=(432, 368))
  i3d_input = tf.placeholder(tf.float32, shape=(None, num_frames_per_clip, crop_size, crop_size, img_channels))
  Y = tf.placeholder(tf.int64, [None])

  with tf.variable_scope('RGB'):
    rgb_model = InceptionI3d(num_classes=num_classes, spatial_squeeze=True, final_endpoint='Logits')
  logits, _ = rgb_model(i3d_input, is_training=False, dropout_keep_prob=dropout_keep_prob)

  fine_tuning_var_map = {}
  restore_weights_map = {}
  for variable in tf.global_variables():
    if variable.name.split('/')[2] == 'Logits' or \
      variable.name.split('/')[2] == 'Mixed_5c' or \
      variable.name.split('/')[2] == 'Mixed_5b':  # or \
      # variable.name.split('/')[2] == 'Mixed_4f' or \
      # variable.name.split('/')[2] == 'Mixed_4e':
      fine_tuning_var_map[variable.name.replace(':0', '')] = variable
    if variable.name.split('/')[2] != 'Logits':
      restore_weights_map[variable.name.replace(':0', '')] = variable
  rgb_saver = tf.train.Saver(var_list=restore_weights_map, reshape=True)
  probabilities = tf.nn.softmax(logits)

  # Define global_step variable and learning rate adjustments
  global_step = tf.Variable(1, trainable=False, dtype=tf.int32, name='global_step')
  learning_rate = tf.train.exponential_decay(initial_learning_rate, global_step, 2000, 0.95, staircase=True)

  # Define loss and optimizer
  loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=Y))
  optimizer = tf.train.AdamOptimizer(learning_rate)

  # Define exponential moving average for all trained variables
  exp_moving_avg = tf.train.ExponentialMovingAverage(0.99)
  variables_averages_op = exp_moving_avg.apply(list(fine_tuning_var_map.values()))
  shadow_ema_vars = {}
  for var in fine_tuning_var_map.values():
    shadow_ema_vars[exp_moving_avg.average_name(var)] = exp_moving_avg.average(var)
  opt_op = optimizer.minimize(loss_op, global_step=global_step, var_list=list(fine_tuning_var_map.values()))
  with tf.control_dependencies([opt_op]):
    train_op = tf.group(variables_averages_op)

  # Evaluation metrics
  predictions = tf.argmax(probabilities, 1)
  correct_pred = tf.equal(predictions, Y)
  accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

  # Summaries
  tf.summary.scalar('accuracy', accuracy)
  tf.summary.scalar('loss', loss_op)
  frame_uint8 = tf.cast(((tf.squeeze(i3d_input[:, num_frames_per_clip // 2 - 1, :, :, :]) + 1.) * 127.), tf.uint8)
  tf.summary.image('samples', frame_uint8, max_outputs=6)
  merged_summary = tf.summary.merge_all()

  with tf.Session() as sess:

    np_data_train = np.load(os.path.join('/home/alessio/Sandbox/NTU', 'lists', 'training_list_anomaly.npy'))
    filenames_train = np_data_train['file']
    labels_train = np_data_train['label']
    lengths_train = np_data_train['frames']
    num_samples_train = np.shape(filenames_train)[0]
    train_idx_dict = {}
    for c in range(num_classes):
      train_idx_dict[c] = np.squeeze(np.argwhere(labels_train == c))

    np_data_test = np.load(os.path.join('/home/alessio/Sandbox/NTU', 'lists', 'testing_list_anomaly.npy'))
    filenames_test = np_data_test['file']
    labels_test = np_data_test['label']
    lengths_test = np_data_test['frames']
    num_samples_test = np.shape(filenames_test)[0]
    test_idx_dict = {}
    for c in range(num_classes):
      test_idx_dict[c] = np.squeeze(np.argwhere(labels_test == c))


    # Add ops to save and restore all the variables.
    classification_saver = tf.train.Saver(var_list=fine_tuning_var_map, max_to_keep=3)
    ema_saver = tf.train.Saver(var_list=shadow_ema_vars, max_to_keep=3)

    # Recover last checkpoints (if any) for classification filters, moving averages, ...
    last_checkpoint_file = tf.train.latest_checkpoint(
        checkpoint_dir=os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova)),
        latest_filename='I3D_finetuning_rgb_' + str(num_prova) + '_latest')
    last_checkpoint_file_ema = tf.train.latest_checkpoint(
        checkpoint_dir=os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova)),
        latest_filename='I3D_finetuning_rgb_ema_' + str(num_prova) + '_latest')

    new_step = 0
    last_step = 0
    if last_checkpoint_file != None:
      last_step = last_checkpoint_file.split('-')[-1]
      print('Found the following checkpoint:', last_checkpoint_file)
      print('Last iteration was', last_step)
      new_step = int(last_step) + 1

    if new_step == 0:
      # Create necessary folders and optionally clear their content
      if os.path.exists(os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova))):
        shutil.rmtree(os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova)))
      os.mkdir(os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova)))
      if os.path.exists(os.path.join(experiments_dir, 'visual_logs/A_train_RGB_' + suffix + str(num_prova))):
        shutil.rmtree(os.path.join(experiments_dir, 'visual_logs/A_train_RGB_' + suffix + str(num_prova)))
      os.mkdir(os.path.join(experiments_dir, 'visual_logs/A_train_RGB_' + suffix + str(num_prova)))
      if os.path.exists(os.path.join(experiments_dir, 'visual_logs/B_test_RGB_' + suffix + str(num_prova))):
        shutil.rmtree(os.path.join(experiments_dir, 'visual_logs/B_test_RGB_' + suffix + str(num_prova)))
      os.mkdir(os.path.join(experiments_dir, 'visual_logs/B_test_RGB_' + suffix + str(num_prova)))

    # Run the initializer
    sess.run(tf.global_variables_initializer())
    sess.run(tf.assign(global_step, new_step))

    # Restore al trained model and last checkpoints (if any)
    rgb_saver.restore(sess, _CHECKPOINT_PATHS['rgb_imagenet'])
    print('RGB branch variables successfully restored.')
    if last_checkpoint_file != None:
      classification_saver.restore(sess, last_checkpoint_file)
      print('Classification variables successfully restored.')
    if last_checkpoint_file_ema != None:
      ema_saver.restore(sess, last_checkpoint_file_ema)
      print('Moving average variables successfully restored.')

    # Prepare summary writers
    train_writer = tf.summary.FileWriter(os.path.join(experiments_dir, 'visual_logs/A_train_RGB_' + suffix + str(num_prova)), sess.graph)
    test_writer = tf.summary.FileWriter(os.path.join(experiments_dir, 'visual_logs/B_test_RGB_' + suffix + str(num_prova)), sess.graph)

    if new_step < final_step:
      training_steps = final_step - new_step

    def prepare_batch(split='train', flip=True):

      if split == 'train':
        video_idx = np.random.choice(num_samples_train, batch_size, replace=False)
      else:
        video_idx = np.random.choice(num_samples_test, batch_size, replace=False)
      # video_idx = []
      # random_class_idx = np.random.choice(num_classes, batch_size, replace=False)
      # for i in range(batch_size):
      #   if split == 'train':
      #     video_idx.append(np.random.choice(train_idx_dict[random_class_idx[i]]))
      #   else:
      #     video_idx.append(np.random.choice(test_idx_dict[random_class_idx[i]]))
      batch_video = np.empty((0, num_frames_per_clip, crop_size, crop_size, 3), dtype=np.float32)
      batch_labels = []
      for i in video_idx:
        if split == 'train':
          video_cube, label, _, _ = pack_frames(filenames_train[i], labels_train[i], lengths_train[i], num_frames_per_clip)
        else:
          video_cube, label, _, _ = pack_frames(filenames_test[i], labels_test[i], lengths_test[i], num_frames_per_clip)
        box = get_box(video_cube, pose_estimator)
        video_cube = video_cube[:, box[0]:box[2], box[1]:box[3], :]
        cube_resized = np.empty((0, crop_size, crop_size, 3), np.float32)
        if flip:
          horizontal_flip = np.random.randint(2)  # Randomly flip the video
        else:
          horizontal_flip = False
        for j in range(num_frames_per_clip):
          frame_resized = cv2.resize(video_cube[j], (crop_size, crop_size))
          frame_resized = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2RGB)
          if horizontal_flip:
            cv2.flip(frame_resized, 0)
          cube_resized = np.concatenate((cube_resized, [frame_resized.astype(np.float32)]), axis=0)
        batch_video = np.concatenate((batch_video, [cube_resized]), axis=0)
        batch_labels.append(label)
      batch_video = batch_video / 127. - 1.

      return batch_video, batch_labels

    for step in range(new_step, new_step + int(training_steps) + 1):

      t1 = time()

      batch_video, batch_labels = prepare_batch(split='train', flip=True)


      sess.run([train_op], feed_dict={i3d_input: batch_video, Y: batch_labels})
      t2 = time()
      print('Step', step, strftime("%H:%M:%S", gmtime()), '(' + str(int(t2 - t1)) + ' s)')

      if step % display_steps == 0 or step == new_step:

        # Calculate batch loss and accuracy
        batch_video, batch_labels = prepare_batch(split='train', flip=False)
        summary, loss, acc = sess.run([merged_summary, loss_op, accuracy], feed_dict={i3d_input: batch_video, Y: batch_labels})
        print("Step " + str(step) + ", Minibatch Loss= " + \
              "{:.4f}".format(loss) + ", Training Accuracy= " + \
              "{:.3f}".format(acc))

        train_writer.add_summary(summary, step)

        batch_video, batch_labels = prepare_batch(split='test', flip=False)

        summary, loss, acc = sess.run([merged_summary, loss_op, accuracy], feed_dict={i3d_input: batch_video, Y: batch_labels})

        print("Step " + str(step) + ", Minibatch Loss= " + \
              "{:.4f}".format(loss) + ", Testing Accuracy= " + \
              "{:.3f}".format(acc))

        test_writer.add_summary(summary, step)

      if step % checkpoint_steps == 0 or step == new_step:

        # Print learning rate (debug)
        print('Learning rate:', learning_rate.eval(session=sess))

        # Save model
        classification_saver.save(
            sess=sess,
            save_path=os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova), 'I3D_finetuning_rgb_' + str(num_prova)),
            global_step=step,
            latest_filename='I3D_finetuning_rgb_' + str(num_prova) + '_latest')
        ema_saver.save(
            sess=sess,
            save_path=os.path.join(experiments_dir, 'checkpoints', suffix + str(num_prova), 'I3D_finetuning_rgb_ema_' + str(num_prova)),
            global_step=step,
            latest_filename='I3D_finetuning_rgb_ema_' + str(num_prova) + '_latest')


if __name__ == "__main__":
  main()