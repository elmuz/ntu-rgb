import numpy as np
import tensorflow as tf


class TfActionEstimator:

  def __init__(self, graph_path, target_size=(32, 224, 224)):
    self.target_size = target_size

    # load graph
    with tf.gfile.GFile(graph_path, 'rb') as f:
      graph_def = tf.GraphDef()
      graph_def.ParseFromString(f.read())

    self.graph = tf.get_default_graph()
    tf.import_graph_def(graph_def, name='TfActionEstimator')

    session_config = tf.ConfigProto()
    session_config.gpu_options.allow_growth = True

    self.persistent_sess = tf.Session(graph=self.graph, config=session_config)
    self.persistent_sess.run(tf.global_variables_initializer())

    self.tensor_input = self.graph.get_tensor_by_name('TfActionEstimator/video:0')
    self.tensor_output = self.graph.get_tensor_by_name('TfActionEstimator/probabilities:0')

    # warm-up
    self.persistent_sess.run(
        self.tensor_output,
        feed_dict={
          self.tensor_input: [
            np.ndarray(shape=(target_size[0], target_size[1], target_size[2], 3), dtype=np.float32)
          ]
        }
    )

  def __del__(self):
    self.persistent_sess.close()

  def inference(self, video_cube):
    return self.persistent_sess.run(self.tensor_output, feed_dict={self.tensor_input: video_cube})
