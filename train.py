from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import matplotlib
import numpy as np
import os
import tensorflow as tf
import input_functions
import model_functions

tf.logging.set_verbosity(tf.logging.INFO)

ENVIRONMENT_PARAMS = {
  'num_reader_threads': 8,
}

EXPERIMENT_PARAMS = {
  'experiment_dir': '/home/alessio/Sandbox/experiments/NTU-RGBD/I3D-RGB',
  'summary_steps': 1000,
  'checkpoint_steps': 1000,

}

TRAINING_PARAMS = {
  'training_steps': 1e5,
  'initial_learning_rate': 2e-4,
  'decay_steps': 3000,
  'decay_rate': 0.95,
  'batch_size': 10,
  'shuffle_batches': 500,
}


def get_params(model, dataset, params=""):
  params_dict = ENVIRONMENT_PARAMS
  params_dict.update(EXPERIMENT_PARAMS)
  params_dict.update(TRAINING_PARAMS)
  params_dict.update(model)
  params_dict.update(dataset)

  hp = tf.contrib.training.HParams(**params_dict)
  #hp.parse(params)

  return hp

def experiment_fn(run_config, hparams):
  estimator = tf.estimator.Estimator(model_fn=model_functions.make_I3D, config=run_config, params=hparams)
  # train_hooks = [
  #   hooks.ExamplesPerSecondHook(
  #     batch_size=hparams.batch_size,
  #     every_n_iter=FLAGS.summary_steps),
  #   hooks.LoggingTensorHook(
  #     collection="batch_logging",
  #     every_n_iter=FLAGS.summary_steps,
  #     batch=True),
  #   hooks.LoggingTensorHook(
  #     collection="logging",
  #     every_n_iter=FLAGS.summary_steps,
  #     batch=False)]
  #eval_hooks = [
  #  hooks.SummarySaverHook(
  #    every_n_iter=FLAGS.summary_steps,
  #    output_dir=os.path.join(run_config.model_dir, "eval"))]

  experiment = tf.contrib.learn.Experiment(
    estimator=estimator,
    train_input_fn=input_functions.make_input_fn(tf.estimator.ModeKeys.TRAIN, hparams),
    eval_input_fn=input_functions.make_input_fn(tf.estimator.ModeKeys.EVAL, hparams),
    train_steps=hparams.training_steps,
    checkpoint_and_export=True,)
    #eval_hooks=eval_hooks)
  #experiment.extend_train_hooks(train_hooks)
  return experiment


def main(unused_argv):

  session_config = tf.ConfigProto()
  session_config.allow_soft_placement = True
  session_config.gpu_options.allow_growth = True
  run_config = tf.contrib.learn.RunConfig(
    model_dir=EXPERIMENT_PARAMS['experiment_dir'],
    save_summary_steps=EXPERIMENT_PARAMS['summary_steps'],
    save_checkpoints_steps=EXPERIMENT_PARAMS['checkpoint_steps'],
    save_checkpoints_secs=None,
    keep_checkpoint_max=3,
    session_config=session_config)

  estimator = tf.contrib.learn.learn_runner.run(
    experiment_fn=experiment_fn,
    run_config=run_config,
    schedule='train_and_evaluate',
    hparams=get_params(model_functions.get_params(), input_functions.get_params()))


if __name__ == "__main__":
  tf.app.run()