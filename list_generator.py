import os
import numpy as np
import cv2

dataset_path = '/home/alessio/Sandbox/NTU'

def create_lists(train_sub=range(1, 31)):

  training = np.array([], dtype=[('file', 'S20'),('label', 'i2'), ('frames', 'i2')])
  testing = np.array([], dtype=[('file', 'S20'),('label', 'i2'), ('frames', 'i2')])

  counter = 0

  for filename in os.listdir(os.path.join(dataset_path, 'nturgb+d_rgb')):

    filename = filename[:20]

    print(filename, counter)
    counter += 1

    subject = int(filename[9:12])
    action = int(filename[17:20]) - 1
    frames = len([name for name in os.listdir(os.path.join(dataset_path, 'frames', filename)) if os.path.isfile(os.path.join(dataset_path, 'frames', filename, name))])

    if subject in train_sub:
      training = np.concatenate((training, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))
    else:
      testing = np.concatenate((testing, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))


  np.save(os.path.join(dataset_path, 'lists','training_list.npy'), training)
  np.save(os.path.join(dataset_path, 'lists', 'testing_list.npy'), testing)


def create_fake_lists():

  # Just for debugging purposes
  training = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])
  testing = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])

  # Repeat 5000 times same input 'S001C001P001R001A001' of 103 frames
  filename = 'S001C001P001R001A001'
  action = 1
  frames = 103
  for i in range(5000):
    training = np.concatenate((training, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))
    testing = np.concatenate((testing, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))
  np.save('/tmp/training_list.npy', training)
  np.save('/tmp/testing_list.npy', testing)


def create_reduced_lists(train_sub=range(1, 31)):

  class_mapping = {
    0: 0,
    1: 0,
    2: 1,
    3: 1,
    4: 2,
    5: 2,
    6: 2,
    7: 3,
    8: 4,
    9: 20,
    10: 5,
    11: 5,
    13: 6,
    14: 6,
    15: 7,
    16: 7,
    17: 8,
    18: 8,
    22: 9,
    27: 10,
    30: 11,
    36: 1,
    40: 12,
    41: 13,
    42: 14,
    43: 15,
    44: 16,
    45: 17,
    46: 18,
    47: 19
  }

  training = []
  testing = []

  training_arr = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])
  testing_arr = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])

  counter = 0

  for filename in os.listdir(os.path.join(dataset_path, 'nturgb+d_rgb')):

    filename = filename[:20]

    print(filename, counter)
    counter += 1

    subject = int(filename[9:12])
    action = int(filename[17:20]) - 1
    frames = len([name for name in os.listdir(os.path.join(dataset_path, 'frames', filename)) if os.path.isfile(os.path.join(dataset_path, 'frames', filename, name))])

    if action in class_mapping.keys():

      if subject in train_sub:
        training.append(filename[:20] + ' ' + str(class_mapping[action]))
        training_arr = np.concatenate((training_arr, np.array([(filename[:20], class_mapping[action], frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))
      else:
        testing.append(filename[:20] + ' ' + str(class_mapping[action]))
        testing_arr = np.concatenate((testing_arr, np.array([(filename[:20], class_mapping[action], frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))


  with open(os.path.join(dataset_path, 'lists', 'training_list_reduced.txt'), 'w') as file_list:
    for item in training:
      file_list.write("%s\n" % item)

  with open(os.path.join(dataset_path, 'lists', 'testing_list_reduced.txt'), 'w') as file_list:
    for item in testing:
      file_list.write("%s\n" % item)

  np.save(os.path.join(dataset_path, 'lists', 'training_list_reduced.npy'), training_arr)
  np.save(os.path.join(dataset_path, 'lists', 'testing_list_reduced.npy'), testing_arr)


def create_lists_for_anomaly(train_sub=list(range(1, 31))):

  training = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])
  testing = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])
  falling = np.array([], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])

  counter = 0

  for filename in os.listdir(os.path.join(dataset_path, 'nturgb+d_rgb')):

    filename = filename[:20]
    print(filename, counter)
    counter += 1

    subject = int(filename[9:12])
    action = int(filename[17:20]) - 1

    if action == 42:  # if action is falling, change ID and append to 'falling' list
      action = 59
    elif action == 59:  # so that training classes are contiguous
      action = 42

    frames = len([name for name in os.listdir(os.path.join(dataset_path, 'frames', filename)) if os.path.isfile(os.path.join(dataset_path, 'frames', filename, name))])

    if action != 59:  # not falling
      if subject in train_sub:
        training = np.concatenate((training, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))
      else:
        testing = np.concatenate((testing, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))
    else:
      falling = np.concatenate((falling, np.array([(filename[:20], action, frames)], dtype=[('file', 'S20'), ('label', 'i2'), ('frames', 'i2')])))

  np.save(os.path.join(dataset_path, 'lists', 'training_list_anomaly.npy'), training)
  np.save(os.path.join(dataset_path, 'lists', 'testing_list_anomaly.npy'), testing)
  np.save(os.path.join(dataset_path, 'lists', 'falling_list_anomaly.npy'), falling)

  print('Training size:', np.shape(training)[0])
  print('Testing size:', np.shape(testing)[0])
  print('Falling size:', np.shape(falling)[0])


def create_lists_txt(train_sub=range(1, 31)):
  """

  Args:
    train_sub: subject IDs of those selected as training set. Originally,
    a 20/20 split had been adopted, where subjects IDs for training were:
    [1, 2, 4, 5, 8, 9, 13, 14, 15, 16, 17, 18, 19, 25, 27, 28, 31, 34, 35, 38].
    This partition led to 40320 samples for training and 16560 for testing.

  Returns:

  """

  training = []
  testing = []

  counter = 0

  for filename in os.listdir(os.path.join(dataset_path, 'nturgb+d_rgb')):

    print(filename, counter)
    counter += 1

    subject = int(filename[9:12])
    action = int(filename[17:20]) - 1

    #if action not in actions:
    #  action = 0

    if subject in train_sub:
      training.append(filename[:20] + ' ' + str(action))
    else:
      testing.append(filename[:20] + ' ' + str(action))

  with open(os.path.join(dataset_path, 'lists','training_list.txt'), 'w') as file_list:
    for item in training:
      file_list.write("%s\n" % item)

  with open(os.path.join(dataset_path, 'lists','testing_list.txt'), 'w') as file_list:
    for item in testing:
      file_list.write("%s\n" % item)


if __name__ == '__main__':
  create_lists_for_anomaly(train_sub=[1, 2, 4, 5, 8, 9, 13, 14, 15, 16, 17, 18, 19, 25, 27, 28, 31, 34, 35, 38])